<?php

abstract class Animal{
    protected $name;
     public function __construct($name)
     {
         $this->name=$name;
     }
     
    abstract public function makeSound();     

    public function getName(){
        return $this->name;
    }
}






// both animal have same behavior "makeSound"

class Dog extends Animal{
    public function makeSound()
    {
        return 'Woof Woof!<br>';
    }
}

class Cat extends Animal{
    public function makeSound()
    {
        return 'Meow Meow!<br>';
    }
}

class Cow extends Animal{
    public function makeSound()
    {
        return 'undefined!<br>';
    }
}

$dog = new Dog('Osold');
echo $dog->getName() . ' : ' . $dog->makeSound();

$cat = new Cat('Tom');
echo $cat->getName() . ' : ' . $cat->makeSound();

$cow = new Cow('Miro');
echo $cow->getName() . ' : ' . $cow->makeSound();

echo '<hr>';

echo 'Interface<br>';


interface Animals{
    public function makeSound();
}


class Dogs implements Animals{

    public function makeSound(){
        return 'Woof!<br>';
    }

}

class Cats implements Animals{

    public function makeSound(){
        return 'Meow!<br>';
    }

}

class Birds implements Animals{

    public function makeSound(){
        return 'Crip!<br>';
    }

}

$dog = new Dogs;
$cat = new Cats;
$bird = new Birds;

echo $dog->makeSound();
echo $cat->makeSound();
echo $bird->makeSound();


//polymorphism => abstract


// create abstract class

abstract class Notification{
    abstract public function notify();
}

class Email extends Notification{
    public function notify()
    {
        echo 'sending mail<br>';
    }
}

class SMS extends Notification{
    public function notify()
    {
        echo 'sending sms<br>';
    }
}


$email = new Email;

$sms = new SMS;

$notificationChannels = ['Email', 'SMS'];

foreach($notificationChannels as $channel)
{
      $notification = new $channel;
      //var_dump($notification);
      $notification->notify();
}

//polymorphism => interface

// create interface

interface Notific{
    public function notify();
}

class Mail implements Notific{
    public function notify()
    {
        echo 'sending mail<br>';
    }
}

class Msg implements Notific{
    public function notify()
    {
        echo 'sending sms<br>';
    }
}


$mail = new Mail;

$msg = new Msg;

$notifications = ['Mail', 'Msg'];

foreach($notifications as $chnl)
{
      $not = new $chnl;
      //var_dump($notification);
      $not->notify();
}